var fs = require('fs');
var spawn = require('child_process').spawn;
var execSync = require('child_process').execSync;
module.exports = function(s,config,lang,onFinish){
    if(config.ffmpegBinary)config.ffmpegDir = config.ffmpegBinary
    var ffmpeg = {};
    var defaults = config.defaults;
    // let defaults = JSON.parse(fs.readFileSync('defaults.json').toString());
    // var defaults = require('defaults.json');
    var platforms = {
        "l4t" : {
            "name" : "NVIDIA Jetson",
            "profiles" : {
                "omx" : {
                    "name" : "OpenMAX Accelerated Plugins",
                    "plugins" : {
                        "encoder" : {
                            "h264" : "omxh264enc",
                            "h265" : "omxh265enc",
                            "vp8" : "omxvp8enc",
                            "vp9" : "omxvp9enc",
                            "jpeg" : "nvjpegenc",
                        },
                        "decoder" : {
                            "h264" : "omxh264dec",
                            "h265" : "omxh265dec",
                            "vp8" : "omxvp8dec",
                            "vp9" : "omxvp9dec",
                            "mpeg2" : "omxmpeg42videodec",
                            "mpeg4" : "omxmpeg4videodec",
                        },
                        "videoconverter" : "nvvidconv",
                        "videoformat" : "nvvidconv",
                        "videoscale" : "nvvidconv",
                        "videorate" : "videorate"
                    }
                },
                "nvv4l2" : {
                    "name" : "NVIDIA® Tegra L4T Accelerated Plugins",
                    "plugins" : {
                        "encoder" : {
                            "h264" : "nvv4l2h264enc",
                            "h265" : "nvv4l2h265enc",
                            "vp8" : "nvv4l2vp8enc",
                            "vp9" : "nvv4l2vp9enc",
                            "jpeg" : "nvjpegenc",
                        },
                        "decoder" : {
                            "h264" : "nvv4l2decoder",
                            "h265" : "nvv4l2decoder",
                            "vp8" : "nvv4l2decoder",
                            "vp9" : "nvv4l2decoder",
                            "mpeg2" : "nvv4l2decoder",
                            "mpeg4" : "nvv4l2decoder",
                        },
                        "videoconverter" : "nvvidconv",
                        "videoformat" : "nvvidconv",
                        "videoscale" : "nvvidconv",
                        "videorate" : "videorate"
                    }
                }
            }
        },
        "generic" : {

        }
    }
    var gstreamer = {
        "use_wrapper" : true,
        "jetson_max_performance" : true,
        "hlssink" : "hlssink2",
        "hlssink_cmd" : "",
        "input_codec" : "",
        "input_name" : "input",
        "decoded" : "false",
        "decoded_name" : "input_decoded",
        "platform" : "l4t",
        "profile" : "omx",
        "send_progress" : true,
    };
    gstreamer.plugins = platforms[gstreamer.platform].profiles[gstreamer.profile].plugins;
    for (var plugin in gstreamer.plugins.decoder) {
        if ((gstreamer.plugins.decoder[plugin] === 'nvv4l2decoder') && (gstreamer.jetson_max_performance)) {
            gstreamer.plugins.decoder[plugin] += ' enable-max-performance=1';
        }
    }
    
    var downloadingFfmpeg = false;

    gstreamer.installed_plugins = {};
    var inspect_stdout = execSync('gst-inspect-1.0').toString().split(/\r{0,1}\n/);

    for (let i = 0; i < inspect_stdout.length; i++) {
        if (!inspect_stdout[i].match(/:.*:/)) {continue}
        plugin = inspect_stdout[i].split(/:\s+/);
        gstreamer.installed_plugins[plugin[1]] = plugin[2];
    }

    if (gstreamer.hlssink === 'hlssink2') {
        gstreamer.hlssink_cmd = 'hlssink2'
    }
    else {
        gstreamer.hlssink_cmd = 'mpegtsmux ! hlssink'
    }

    

    //check local ffmpeg
    ffmpeg.checkForWindows = function(failback){
        if (s.isWin && fs.existsSync(s.mainDirectory+'/ffmpeg/ffmpeg.exe')) {
            config.ffmpegDir = s.mainDirectory+'/ffmpeg/ffmpeg.exe'
        }else{
            failback()
        }
    }
    //check local ffmpeg
    ffmpeg.checkForUnix = function(failback){
        if(s.isWin === false){
            
            if (fs.existsSync('/usr/bin/gst-launch-1.0')) {
                config.gstDir = '/usr/bin/gst-launch-1.0'
            }else{
                if (fs.existsSync('/usr/bin/gst-launch-1.0')) {
                    config.gstDir = '/usr/bin/gst-launch-1.0'
                }else{
                    failback()
                }
            }
            // Kept of competability with monitor.js hardcoded commands & waiting for https://gitlab.freedesktop.org/gstreamer/gst-plugins-good/issues/569
            if (fs.existsSync('/usr/bin/ffmpeg')) {
                config.ffmpegDir = '/usr/bin/ffmpeg'
            }else{
                if (fs.existsSync('/usr/local/bin/ffmpeg')) {
                    config.ffmpegDir = '/usr/local/bin/ffmpeg'
                }else{
                    failback()
                }
            }
            if (gstreamer.use_wrapper === true) {
                config.gstreamerPath = config.gstDir;
                config.gstDir = '/usr/bin/node';
            }
        }else{
            failback()
        }
    }
    //check node module : ffmpeg-static
    ffmpeg.checkForNpmStatic = function(failback){
        config.gstDir = '/usr/bin/gst-launch-1.0'
        if (gstreamer.use_wrapper === true) {
            config.gstreamerPath = config.gstDir;
            config.gstDir = '/usr/bin/node';
        }
        try{
            var staticFFmpeg = require('ffmpeg-static').path;
            if (fs.statSync(staticFFmpeg)) {
                config.ffmpegDir = staticFFmpeg
            }else{
                console.log('"ffmpeg-static" from NPM has failed to provide a compatible library or has been corrupted.')
                console.log('Run "npm uninstall ffmpeg-static" to remove it.')
                console.log('Run "npm install ffbinaries" to get a different static FFmpeg downloader.')
            }
        }catch(err){
            console.log('No "ffmpeg-static".')
            failback()
        }
    }
    //check node module : ffbinaries
    ffmpeg.checkForFfbinary = function(failback){
        config.gstDir = '/usr/bin/gst-launch-1.0'
        if (gstreamer.use_wrapper === true) {
            config.gstreamerPath = config.gstDir;
            config.gstDir = '/usr/bin/node';
        }
        try{
            ffbinaries = require('ffbinaries')
            var ffbinaryDir = s.mainDirectory + '/ffmpeg/'
            var downloadFFmpeg = function(){
                downloadingFfmpeg = true
                console.log('ffbinaries : Downloading FFmpeg. Please Wait...');
                ffbinaries.downloadBinaries(['ffmpeg', 'ffprobe'], {
                    destination: ffbinaryDir,
                    version : '3.4'
                },function () {
                    config.ffmpegDir = ffbinaryDir + 'ffmpeg'
                    console.log('ffbinaries : FFmpeg Downloaded.');
                    ffmpeg.completeCheck()
                })
            }
            if (!fs.existsSync(ffbinaryDir + 'ffmpeg')) {
                downloadFFmpeg()
            }else{
                config.ffmpegDir = ffbinaryDir + 'ffmpeg'
            }
        }catch(err){
            console.log('No "ffbinaries". Continuing.')
            console.log('Run "npm install ffbinaries" to get this static FFmpeg downloader.')
            failback()
        }
    }
    //ffmpeg version
    ffmpeg.checkVersion = function(callback){
        try{
            s.ffmpegVersion = execSync(config.ffmpegDir+" -version").toString().split('Copyright')[0].replace('ffmpeg version','').trim()
            if(s.ffmpegVersion.indexOf(': 2.')>-1){
                s.systemLog('FFMPEG is too old : '+s.ffmpegVersion+', Needed : 3.2+',err)
                throw (new Error())
            }
        }catch(err){
            console.log('No FFmpeg found.')
            // process.exit()
        }
        callback()
    }
    //check available hardware acceleration methods
    ffmpeg.checkHwAccelMethods = function(callback){
        // if(config.availableHWAccels === undefined){
            // hwAccels = execSync(config.ffmpegDir+" -loglevel quiet -hwaccels").toString().split('\n')
            // hwAccels.shift()
            // availableHWAccels = []
            // hwAccels.forEach(function(method){
                // if(method && method !== '')availableHWAccels.push(method.trim())
            // })
            // config.availableHWAccels = availableHWAccels
            // config.availableHWAccels = ['auto'].concat(config.availableHWAccels)
            // console.log('Available Hardware Acceleration Methods : ',availableHWAccels.join(', '))
            // var methods = {
                // auto: {label:lang['Auto'],value:'auto'},
                // drm: {label:lang['drm'],value:'drm'},
                // cuvid: {label:lang['cuvid'],value:'cuvid'},
                // vaapi: {label:lang['vaapi'],value:'vaapi'},
                // qsv: {label:lang['qsv'],value:'qsv'},
                // vdpau: {label:lang['vdpau'],value:'vdpau'},
                // dxva2: {label:lang['dxva2'],value:'dxva2'},
                // vdpau: {label:lang['vdpau'],value:'vdpau'},
                // videotoolbox: {label:lang['videotoolbox'],value:'videotoolbox'}
            // }
            // s.listOfHwAccels = []
            // config.availableHWAccels.forEach(function(availibleMethod){
                // if(methods[availibleMethod]){
                    // var method = methods[availibleMethod]
                    // s.listOfHwAccels.push({
                        // name: method.label,
                        // value: method.value,
                    // })
                 // }
            // })
        // }
        callback()
    }
    ffmpeg.completeCheck = function(){
        ffmpeg.checkVersion(function(){
            ffmpeg.checkHwAccelMethods(function(){
                s.onFFmpegLoadedExtensions.forEach(function(extender){
                    extender(ffmpeg)
                })
                onFinish(ffmpeg)
            })
        })
    }
    
    //ffmpeg string cleaner, splits for use with spawn()
    s.splitForFFPMEG = function (ffmpegCommandAsString) {
        //this function ignores spaces inside quotes.
        return ffmpegCommandAsString.replace(/\s+/g,' ').trim().match(/\\?.|^$/g).reduce((p, c) => {
            if(c === '"'){
                p.quote ^= 1;
            }else if(!p.quote && c === ' '){
                p.a.push('');
            }else{
                p.a[p.a.length-1] += c.replace(/\\(.)/,"$1");
            }
            return  p;
        }, {a: ['']}).a
    };

    s.mergeDefaults = function (v, d) {
        var value;
        Object.keys(d).some(function(k) {
            if (!v[k]) {
                v[k]=d[k];
            }
            else if (typeof d[k] === 'object') {
                v[k] = s.mergeDefaults(v[k], d[k]);
            }
        });
        return v;
    }
    // s.createFFmpegMap = function(e,arrayOfMaps){
    //     //`e` is the monitor object
    //     var string = '';
    //     if(e.details.input_maps && e.details.input_maps.length > 0){
    //         if(arrayOfMaps && arrayOfMaps instanceof Array && arrayOfMaps.length>0){
    //             arrayOfMaps.forEach(function(v){
    //                 if(v.map==='')v.map='0'
    //                 string += ' -map '+v.map
    //             })
    //         }else{
    //             var primaryMap = '0:0'
    //             if(e.details.primary_input && e.details.primary_input !== '')primaryMap = e.details.primary_input
    //             string += ' -map ' + primaryMap
    //         }
    //     }
    //     return string;
    // }

    //create sub stream channel
    // s.createStreamChannel = function(e,number,channel){
    //     //`e` is the monitor object
    //     //`x` is an object used to contain temporary values.
    //     var x = {
    //         pipe: '',
    //         cust_stream: ''
    //     }
    //     if(!number||number==''){
    //         x.channel_sdir = e.sdir;
    //     }else{
    //         x.channel_sdir = e.sdir+'channel'+number+'/';
    //         if (!fs.existsSync(x.channel_sdir)){
    //             fs.mkdirSync(x.channel_sdir);
    //         }
    //     }
    //     x.stream_video_filters=[]
    //     //stream - frames per second
    //     if(channel.stream_vcodec!=='copy'){
    //         if(!channel.stream_fps||channel.stream_fps===''){
    //             switch(channel.stream_type){
    //                 case'rtmp':
    //                     channel.stream_fps=30
    //                 break;
    //                 default:
    // //                        channel.stream_fps=5
    //                 break;
    //             }
    //         }
    //     }
    //     if(channel.stream_fps&&channel.stream_fps!==''){x.stream_fps=' -r '+channel.stream_fps}else{x.stream_fps=''}

    //     //stream - hls vcodec
    //     if(channel.stream_vcodec&&channel.stream_vcodec!=='no'){
    //         if(channel.stream_vcodec!==''){x.stream_vcodec=' -c:v '+channel.stream_vcodec}else{x.stream_vcodec=' -c:v libx264'}
    //     }else{
    //         x.stream_vcodec='';
    //     }
    //     //stream - hls acodec
    //     if(channel.stream_acodec!=='no'){
    //     if(channel.stream_acodec&&channel.stream_acodec!==''){x.stream_acodec=' -c:a '+channel.stream_acodec}else{x.stream_acodec=''}
    //     }else{
    //         x.stream_acodec=' -an';
    //     }
    //     //stream - resolution
        // if(channel.stream_scale_x&&channel.stream_scale_x!==''&&channel.stream_scale_y&&channel.stream_scale_y!==''){
        //     x.dimensions = channel.stream_scale_x+'x'+channel.stream_scale_y;
        // }
    //     //stream - hls segment time
    //     if(channel.hls_time&&channel.hls_time!==''){x.hls_time=channel.hls_time}else{x.hls_time="2"}
    //     //hls list size
    //     if(channel.hls_list_size&&channel.hls_list_size!==''){x.hls_list_size=channel.hls_list_size}else{x.hls_list_size=2}
    //     //stream - custom flags
    //     if(channel.cust_stream&&channel.cust_stream!==''){x.cust_stream=' '+channel.cust_stream}
    //     //stream - preset
    //     if(channel.stream_type !== 'h265' && channel.preset_stream && channel.preset_stream!==''){x.preset_stream=' -preset '+channel.preset_stream;}else{x.preset_stream=''}
    //     //hardware acceleration
    //     if(e.details.accelerator&&e.details.accelerator==='1'){
    //         if(e.details.hwaccel === 'auto')e.details.hwaccel = ''
    //         if(e.details.hwaccel && e.details.hwaccel!==''){
    //             x.hwaccel+=' -hwaccel '+e.details.hwaccel;
    //         }
    //         if(e.details.hwaccel_vcodec&&e.details.hwaccel_vcodec!==''){
    //             x.hwaccel+=' -c:v '+e.details.hwaccel_vcodec;
    //         }
    //         if(e.details.hwaccel_device&&e.details.hwaccel_device!==''){
    //             switch(e.details.hwaccel){
    //                 case'vaapi':
    //                     x.hwaccel+=' -vaapi_device '+e.details.hwaccel_device+' -hwaccel_output_format vaapi';
    //                 break;
    //                 default:
    //                     x.hwaccel+=' -hwaccel_device '+e.details.hwaccel_device;
    //                 break;
    //             }
    //         }
    // //        else{
    // //            if(e.details.hwaccel==='vaapi'){
    // //                x.hwaccel+=' -hwaccel_device 0';
    // //            }
    // //        }
    //     }

    //     if(channel.rotate_stream&&channel.rotate_stream!==""&&channel.rotate_stream!=="no"){
    //         x.stream_video_filters.push('transpose='+channel.rotate_stream);
    //     }
    //     //stream - video filter
    //     if(channel.svf&&channel.svf!==''){
    //         x.stream_video_filters.push(channel.svf)
    //     }
    //     if(x.stream_video_filters.length>0){
    //         var string = x.stream_video_filters.join(',').trim()
    //         if(string===''){
    //             x.stream_video_filters=''
    //         }else{
    //             x.stream_video_filters=' -vf '+string
    //         }
    //     }else{
    //         x.stream_video_filters=''
    //     }
    //     if(e.details.input_map_choices&&e.details.input_map_choices.record){
    //         //add input feed map
    //         x.pipe += s.createFFmpegMap(e,e.details.input_map_choices['stream_channel-'+(number-config.pipeAddition)])
    //     }
    //     if(channel.stream_vcodec !== 'copy' || channel.stream_type === 'mjpeg' || channel.stream_type === 'b64'){
    //         x.cust_stream += x.stream_fps
    //     }
    //     switch(channel.stream_type){
    //         case'mp4':
    //             x.cust_stream+=' -movflags +frag_keyframe+empty_moov+default_base_moof -metadata title="Poseidon Stream" -reset_timestamps 1'
    //             if(channel.stream_vcodec!=='copy'){
    //                 if(x.dimensions && x.cust_stream.indexOf('-s ')===-1){x.cust_stream+=' -s '+x.dimensions}
    //                 if(channel.stream_quality && channel.stream_quality !== '')x.cust_stream+=' -crf '+channel.stream_quality;
    //                 x.cust_stream+=x.preset_stream
    //                 x.cust_stream+=x.stream_video_filters
    //             }
    //             x.pipe+=' -f mp4'+x.stream_acodec+x.stream_vcodec+x.cust_stream+' pipe:'+number;
    //         break;
    //         case'rtmp':
    //             x.rtmp_server_url=s.checkCorrectPathEnding(channel.rtmp_server_url);
    //             if(channel.stream_vcodec!=='copy'){
    //                 if(channel.stream_vcodec==='libx264'){
    //                     channel.stream_vcodec = 'h264'
    //                 }
    //                 if(channel.stream_quality && channel.stream_quality !== '')x.cust_stream+=' -crf '+channel.stream_quality;
    //                 x.cust_stream+=x.preset_stream
    //                 if(channel.stream_v_br&&channel.stream_v_br!==''){x.cust_stream+=' -b:v '+channel.stream_v_br}
    //             }
    //             if(channel.stream_vcodec!=='no'&&channel.stream_vcodec!==''){
    //                 x.cust_stream+=' -vcodec '+channel.stream_vcodec
    //             }
    //             if(channel.stream_acodec!=='copy'){
    //                 if(!channel.stream_acodec||channel.stream_acodec===''||channel.stream_acodec==='no'){
    //                     channel.stream_acodec = 'aac'
    //                 }
    //                 if(!channel.stream_a_br||channel.stream_a_br===''){channel.stream_a_br='128k'}
    //                 x.cust_stream+=' -ab '+channel.stream_a_br
    //             }
    //             if(channel.stream_acodec!==''){
    //                 x.cust_stream+=' -acodec '+channel.stream_acodec
    //             }
    //             x.pipe+=' -f flv'+x.stream_video_filters+x.cust_stream+' "'+x.rtmp_server_url+channel.rtmp_stream_key+'"';
    //         break;
    //         case'h264':
    //             if(channel.stream_vcodec!=='copy'){
    //                 if(x.dimensions && x.cust_stream.indexOf('-s ')===-1){x.cust_stream+=' -s '+x.dimensions}
    //                 if(channel.stream_quality && channel.stream_quality !== '')x.cust_stream+=' -crf '+channel.stream_quality;
    //                 x.cust_stream+=x.preset_stream
    //                 x.cust_stream+=x.stream_video_filters
    //             }
    //             x.pipe+=' -f mpegts'+x.stream_acodec+x.stream_vcodec+x.cust_stream+' pipe:'+number;
    //         break;
    //         case'flv':
    //             if(channel.stream_vcodec!=='copy'){
    //                 if(x.dimensions && x.cust_stream.indexOf('-s ')===-1){x.cust_stream+=' -s '+x.dimensions}
    //                 if(channel.stream_quality && channel.stream_quality !== '')x.cust_stream+=' -crf '+channel.stream_quality;
    //                 x.cust_stream+=x.preset_stream
    //                 x.cust_stream+=x.stream_video_filters
    //             }
    //             x.pipe+=' -f flv'+x.stream_acodec+x.stream_vcodec+x.cust_stream+' pipe:'+number;
    //         break;
    //         case'hls':
    //             if(channel.stream_vcodec!=='h264_vaapi'&&channel.stream_vcodec!=='copy'){
    //                 if(channel.stream_quality && channel.stream_quality !== '')x.cust_stream+=' -crf '+channel.stream_quality;
    //                 if(x.cust_stream.indexOf('-tune')===-1){x.cust_stream+=' -tune zerolatency'}
    //                 if(x.cust_stream.indexOf('-g ')===-1){x.cust_stream+=' -g 1'}
    //                 if(x.dimensions && x.cust_stream.indexOf('-s ')===-1){x.cust_stream+=' -s '+x.dimensions}
    //                 x.cust_stream+=x.stream_video_filters
    //             }
    //             x.pipe+=x.preset_stream+x.stream_acodec+x.stream_vcodec+' -f hls'+x.cust_stream+' -hls_time '+x.hls_time+' -hls_list_size '+x.hls_list_size+' -start_number 0 -hls_allow_cache 0 -hls_flags +delete_segments+omit_endlist "'+x.channel_sdir+'s.m3u8"';
    //         break;
    //         case'mjpeg':
    //             if(channel.stream_quality && channel.stream_quality !== '')x.cust_stream+=' -q:v '+channel.stream_quality;
    //             if(x.dimensions && x.cust_stream.indexOf('-s ')===-1){x.cust_stream+=' -s '+x.dimensions}
    //             x.pipe+=' -c:v mjpeg -f mpjpeg -boundary_tag shinobi'+x.cust_stream+x.stream_video_filters+' pipe:'+number;
    //         break;
    //         default:
    //             x.sink=''
    //         break;
    //     }
    //     return x.pipe
    // }
    function isArray(o) {
        if (Object.prototype.toString.call(o) == '[object Array]') {
            return true;
        }
        return false;
    }
    gstreamer.discoverStream = function(e,x){
        const { execSync } = require('child_process');
        let stdout = execSync('gst-discoverer-1.0 -v \''+e.url+'\'').toString();
        var discovered = {};

        if (e.details.discovery_parse_simple==="1") {
            discovered = gstreamer.parseDiscovererSimple(e,x,stdout);
        }
        else {
            discovered = gstreamer.parseDiscoverer(e,x,stdout);
        }

        if (discovered.container) {
            gstreamer.parseCaps(e,x,discovered.container);
        }
        if (discovered.video) {
            gstreamer.parseCaps(e,x,discovered.video);
        }
        if (discovered.audio) {
            gstreamer.parseCaps(e,x,discovered.audio);
        }
        return discovered;
    }
    gstreamer.parseDiscovererSimple = function(e,x,stdout){
        // TODO: Need to find a better way to parse or find stream info.
        discovered = {};
        container = stdout.match(/ *container: {0,1}([^\n\r]*)/);
        if (container) {
            discovered.container = {};
            discovered.container.Caps=container[1];
        }
        video = stdout.match(/ *video: {0,1}([^\n\r]*)/);
        if (video) {
            discovered.video = {};
            discovered.video.Caps=video[1];
        }
        else {
            console.log('Unable to discover video capabilities from '+e.protocol+'://'+e.host+e.path);
        }
        audio = stdout.match(/ *audio: {0,1}([^\n\r]*)/);
        if (audio) {
            discovered.audio = {};
            discovered.audio.Caps=audio[1];
        }
        return discovered;
    }
    gstreamer.parseDiscoverer = function(e,x,stdout){
        // this parsing method is experimental. the amount of assumptions used here increases the risk of breaking
        let discoverer_lines = stdout.split(/\r{0,1}\n/);
        var discovered = {'_name' : 'root'};
        indent = 0;
        indent_previous = 0;
        property_previous = 'root';
        process_stdout = false;
        unknown = '';
        var level = {
            '0' : discovered
        };
        for (let i = 0; i < discoverer_lines.length; i++) {
            if (!process_stdout&&discoverer_lines[i]==='Topology:') {
                process_stdout = true;
            }
            if (process_stdout) {
              discoverer_info = discoverer_lines[i].match(/( *)([^ ][^:]+): {0,1}([^\n]*)$/);
              if (property_previous==='Codec') {
                parent[property_previous]=discoverer_lines[i].match(/ *([^ ][^\n]+)$/)[1];
                property_previous=''
              }
              if (!discoverer_info) {continue}
              if (discoverer_info[2]==='unknown') {
                unknown = discoverer_info[3];
                continue;
              }
              indent = discoverer_info[1].length;
              if (indent_previous < indent) {
                if (isArray(parent[property_previous])) {
                  level[indent] = parent_new;
                }
                else if (typeof parent[property_previous]!=="object") {
                  caps = parent[property_previous];
                  parent[property_previous] = {
                    '_parent' : parent['_name'],
                    '_name' : property_previous,
                    'Caps' : caps
                  };
                  if (level[indent]) {delete level[indent]}
                  level[indent] = parent[property_previous];
                }
                else {
                  level[indent] = parent[property_previous];
                }
            }
              parent = level[indent];
                if (discoverer_info[3].length === 0) {
                  value = {'_parent' : parent['_name'], '_name' : discoverer_info[2]};
                }
                else {
                    value = discoverer_info[3];
                }
                if ((parent['_name'] === 'container')||(parent['_name'] === 'Topology'&&discoverer_info[2]!=='container')) {
                    parent_new = {
                    '_parent' : 'container',
                    '_name' : discoverer_info[2],
                    'Caps' : value
                  }
                  if (unknown!=='') {
                    parent_new['Packet Caps'] = unknown;
                    unknown = '';
                  }
                  if (parent[discoverer_info[2]]) {
                    parent[discoverer_info[2]].push(parent_new);
                  }
                  else {
                    parent[discoverer_info[2]] = [parent_new];
                  }
                }
                else {
                    parent[discoverer_info[2]] = value;
                }
                property_previous = discoverer_info[2];
                indent_previous = indent;
            }
        }
        if (!process_stdout) {
            console.log('Unable to open stream '+e.protocol+'://'+e.host+e.path);
        }
        // For now, only the first single video or audio streams per source
        if (discovered.Topology) {
            if (discovered.Topology.container) {
                if (discovered.Topology.container.video&&isArray(discovered.Topology.container.video)) {
                    discovered['video']=discovered.Topology.container.video[0];
                }
                if (discovered.Topology.container.audio&&isArray(discovered.Topology.container.audio)) {
                    discovered['audio']=discovered.Topology.container.audio[0];
                }
            }
            else if (discovered.Topology.video&&isArray(discovered.Topology.video)) {
                discovered['video']=discovered.Topology.video[0];
                if (discovered.Topology.audio&&isArray(discovered.Topology.audio)) {
                    discovered['audio']=discovered.Topology.audio[0];
                }
            }
        }
        else {
            console.log('Unable to discover stream information from '+e.protocol+'://'+e.host+e.path);
        }
        return discovered;
    }
    
    gstreamer.parseCaps = function(e,x,stream){
        stream.Caps_parsed = {};
        caps = stream.Caps.split(', ');
        stream.Caps_parsed = {};
        stream.Caps_parsed.type = caps[0];
        stream.Caps_parsed.type_simple = caps[0].split('/')[1].replace(/^x-/,'');
        
        for (let i = 1; i < caps.length; i++) {
            caps_split = caps[i].split(/=\([^\)]+\)/);
            stream.Caps_parsed[caps_split[0]] = caps_split[1];
        }
        if (stream.Caps_parsed.framerate) {
            stream.Caps_parsed.fps = stream.Caps_parsed.framerate.split('/')[0];
        }
    }

    gstreamer.buildMainInput = function(e,x){
        //e = monitor object
        //x = temporary values
        
        x.main_input_prop='';
        x.output = {};

        e.details = s.mergeDefaults(e.details, defaults.monitor);
        
        if (e.details.discovery_enable==='1') {
            x.discovered = gstreamer.discoverStream(e,x);
            if (x.discovered&&x.discovered.video&&x.discovered.video.Caps_parsed) {
                x.input_vcodec=x.discovered.video.Caps_parsed.type_simple;
                x.input_fps=x.discovered.video.Caps_parsed.fps;
                x.input_width=x.discovered.video.Caps_parsed.width;
                x.input_height=x.discovered.video.Caps_parsed.height;
            }
            if (x.discovered.audio&&x.discovered.audio.Caps_parsed) {
                x.input_acodec = x.discovered.audio.Caps_parsed.type_simple;
            }
        }
        else {
            x.input_vcodec=e.type;
            x.input_fps=e.fps;
            x.input_width=e.width;
            x.input_height=e.height;
            
        }

        e.isStreamer = (e.type === 'dashcam'|| e.type === 'socket')
        e.coProcessor = false
        if(
            e.details.use_coprocessor === '1' &&
            e.details.accelerator === '1' &&
            e.isStreamer === false &&
            (!e.details.input_maps || e.details.input_maps.length === 0) &&
            (e.details.snap === '1' || e.details.stream_type === 'mjpeg' || e.details.stream_type === 'b64' || e.details.detector === '1')
          ){
            e.coProcessor = true
        }else if(e.details.accelerator === '1' && e.details.hwaccel === 'cuvid' && e.details.hwaccel_vcodec === ('h264_cuvid' || 'hevc_cuvid' || 'mjpeg_cuvid' || 'mpeg4_cuvid')){
            e.cudaEnabled = true
        }
        //
        x.hwaccel = '';
        x.main_input = '';
        gstreamer.input_parser = '';
        gstreamer.input_parser_tee = '';
        gstreamer.queue_properties = ' max-size-time=0 max-size-bytes=0 max-size-buffers=0';
        //input - frame rate (capture rate)
        // if(e.details.sfps && e.details.sfps!==''){x.input_fps=' -r '+e.details.sfps}else{x.input_fps=''}

        //input - analyze duration
        // if(e.details.aduration&&e.details.aduration!==''){x.cust_input+=' -analyzeduration '+e.details.aduration};

        //input - probe size
        // if(e.details.probesize&&e.details.probesize!==''){x.cust_input+=' -probesize '+e.details.probesize};

        
        
        //input
        // if(e.details.cust_input.indexOf('-fflags') === -1){x.cust_input+=' -fflags +igndts'}
        // gstreamer.input_codec = 'h264';
        switch(e.type){
            case'dashcam':
                x.main_input='fdsrc';
                // x.ffmpegCommandString += x.cust_input+x.hwaccel+' -i -';
            break;
            case'socket':case'jpeg':case'pipe'://case'webpage':
                x.main_input='fdsrc';
            //     x.ffmpegCommandString += ' -pattern_type glob -f image2pipe'+x.record_fps+' -vcodec mjpeg'+x.cust_input+x.hwaccel+' -i -';
            break;
            case'mjpeg':
                x.main_input='souphttpsrc retries=-1 location='+e.url;
            //     x.ffmpegCommandString += ' -reconnect 1 -f mjpeg'+x.cust_input+x.hwaccel+' -i "'+e.url+'"';
            break;
            case'mxpeg':
                x.main_input='souphttpsrc retries=-1 location='+e.url;
            break;
            case'rtmp':
                x.main_input=`rtmpsrc location='rtmp://127.0.0.1:1935/${e.ke + '_' + e.mid + '_' + e.details.rtmp_key}'`;
            break;
            case'h264':
                x.input_parse = '';
                switch(e.protocol){
                    case'rtsp':
                        if(e.details.rtsp_transport&&e.details.rtsp_transport!==''&&e.details.rtsp_transport!=='no'){
                            parser = x.input_vcodec+'parse';
                            if (!gstreamer.installed_plugins[parser]) {
                                console.log('Unable to find Gstreamer plugin "'+parser+'"');
                            }
                            if (gstreamer.parse_pre_tee) {
                                gstreamer.input_parser=' ! '+x.input_vcodec+'parse';
                            }
                            else {
                                gstreamer.input_parser_tee=' ! '+x.input_vcodec+'parse';
                            }
                            x.backchannel='';
                            if (e.details.is_onvif==='1') {x.backchannel=' backchannel=1'}
                            // video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.2, profile=(string)main, width=(int)1920, height=(int)1080, framerate=(fraction)25/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, parsed=(boolean)true
                            x.main_input='rtspsrc location=\''+e.url
                                        +'\' latency='+e.details.rtsp_latency
                                        +x.backchannel
                                        +' ! rtp'+x.input_vcodec+'depay'+gstreamer.input_parser;
                        }
                    break;
                }
            break;
            case'mp4':
                //input - stream loop (good for static files/lists)
                if(e.details.stream_loop === '1'){x.main_input_prop=' loop=true'};
                // TODO: Error handling for no caps discovered
                x.main_input='multifilesrc location=\''+e.url+'\''+x.main_input_prop+' index=0 caps=\''+x.input_stream.video;
            break;
            case'hls':
                x.main_input='souphttpsrc location='+e.url+' ! hlsdemux';
            break;
            case'local':
                if(e.details.stream_loop === '1'){x.main_input_prop=' loop=true'};
                // TODO: Error handling for no caps discovered
                x.main_input='multifilesrc location=\''+e.path+'\''+x.main_input_prop+' index=0 caps=\''+x.input_stream.video;
            break;
        }
        
        if (gstreamer.send_progress) {
            x.main_input+=' ! progressreport update-freq=1 silent=true';
        }

        //logging - level
        gst_debug_level=0;
        if(e.details.loglevel&&e.details.loglevel!==''){
            switch(e.details.loglevel){
                case'error':
                        gst_debug_level=1;
                break;
                case'warning':
                        gst_debug_level=2;
                break;
                case'fixme':
                        gst_debug_level=3;
                break;
                case'info':
                        gst_debug_level=4;
                break;
                case'debug':
                        gst_debug_level=5;
                break;
                case'log':
                        gst_debug_level=6;
                break;
                case'trace':
                        gst_debug_level=7;
                break;
            }
        }
        x.loglevel='GST_DEBUG='+gst_debug_level;
        //custom - input flags
        gstreamer.input_start = gstreamer.input_name+'. ! queue'+gstreamer.queue_properties+gstreamer.input_parser_tee;
        gstreamer.input_decoded_start=gstreamer.decoded_name+'. ! queue'+gstreamer.queue_properties;

        if(e.details.cust_input&&e.details.cust_input!==''){x.main_input=e.details.cust_input;}
        //add main input
        x.main_input+=' ! tee name='+gstreamer.input_name+'';
        // if((e.type === 'mp4' || e.type === 'mjpeg') && x.main_input.indexOf('-re') === -1){
        //     x.cust_input += ' -re'
        // }
    }
    gstreamer.buildMainStream = function(e,x){
        //e = monitor object
        //x = temporary values
        x.stream_video_filters = [];
        x.output.main_stream = {};
        x.output.stream_image = {};
        x.cust_stream = '';
        x.scale = '';
        main_stream = '';
        x.stream_image='';
        //stream - timestamp
        if(e.details.stream_timestamp&&e.details.stream_timestamp=="1"&&e.details.vcodec!=='copy'){

            //font
            // if(e.details.stream_timestamp_font&&e.details.stream_timestamp_font!==''){x.stream_timestamp_font=e.details.stream_timestamp_font}else{x.stream_timestamp_font='/usr/share/fonts/truetype/freefont/FreeSans.ttf'}
            // //position x
            // if(e.details.stream_timestamp_x&&e.details.stream_timestamp_x!==''){x.stream_timestamp_x=e.details.stream_timestamp_x}else{x.stream_timestamp_x='(w-tw)/2'}
            // //position y
            // if(e.details.stream_timestamp_y&&e.details.stream_timestamp_y!==''){x.stream_timestamp_y=e.details.stream_timestamp_y}else{x.stream_timestamp_y='0'}
            // //text color
            // if(e.details.stream_timestamp_color&&e.details.stream_timestamp_color!==''){x.stream_timestamp_color=e.details.stream_timestamp_color}else{x.stream_timestamp_color='white'}
            // //box color
            // if(e.details.stream_timestamp_box_color&&e.details.stream_timestamp_box_color!==''){x.stream_timestamp_box_color=e.details.stream_timestamp_box_color}else{x.stream_timestamp_box_color='0x00000000@1'}
            // //text size
            // if(e.details.stream_timestamp_font_size&&e.details.stream_timestamp_font_size!==''){x.stream_timestamp_font_size=e.details.stream_timestamp_font_size}else{x.stream_timestamp_font_size='10'}
            // x.stream_video_filters.push('drawtext="fontfile='+x.stream_timestamp_font+':text=\'%{localtime}\':x='+x.stream_timestamp_x+':y='+x.stream_timestamp_y+':fontcolor='+x.stream_timestamp_color+':box=1:boxcolor='+x.stream_timestamp_box_color+':fontsize='+x.stream_timestamp_font_size + '"');
        }
        //stream - watermark for -vf
        if(e.details.stream_watermark&&e.details.stream_watermark=="1"&&e.details.stream_watermark_location&&e.details.stream_watermark_location!==''){
            // switch(e.details.stream_watermark_position){
            //     case'tl'://top left
            //         x.stream_watermark_position='10:10'
            //     break;
            //     case'tr'://top right
            //         x.stream_watermark_position='main_w-overlay_w-10:10'
            //     break;
            //     case'bl'://bottom left
            //         x.stream_watermark_position='10:main_h-overlay_h-10'
            //     break;
            //     default://bottom right
            //         x.stream_watermark_position='(main_w-overlay_w-10)/2:(main_h-overlay_h-10)/2'
            //     break;
            // }
            // x.stream_video_filters.push('movie='+e.details.stream_watermark_location+'[watermark],[in][watermark]overlay='+x.stream_watermark_position+'[out]');
        }
        //stream - rotation
        // if(e.details.rotate_stream&&e.details.rotate_stream!==""&&e.details.rotate_stream!=="no"&&e.details.stream_vcodec!=='copy'){
        //     x.stream_video_filters.push('transpose='+e.details.rotate_stream);
        // }
        //stream - hls vcodec
        
        // if(e.details.stream_vcodec&&e.details.stream_vcodec!=='no'){
        //     if(e.details.stream_vcodec!==''){x.stream_vcodec=' -c:v '+e.details.stream_vcodec}else{x.stream_vcodec=' -c:v libx264'}
        // }else{
        //     x.stream_vcodec='';
        // }
        // //stream - hls acodec
        // if(e.details.stream_acodec!=='no'){
        // if(e.details.stream_acodec&&e.details.stream_acodec!==''){x.stream_acodec=' -c:a '+e.details.stream_acodec}else{x.stream_acodec=''}
        // }else{
        //     x.stream_acodec=' -an';
        // }
        // //stream - hls segment time
        // if(e.details.hls_time&&e.details.hls_time!==''){x.hls_time=e.details.hls_time}else{x.hls_time="2"}    //hls list size
        // if(e.details.hls_list_size&&e.details.hls_list_size!==''){x.hls_list_size=e.details.hls_list_size}else{x.hls_list_size=2}
        // //stream - custom flags
        // if(e.details.cust_stream&&e.details.cust_stream!==''){x.cust_stream=' '+e.details.cust_stream}
        // //stream - preset
        // if(e.details.stream_type !== 'h265' && e.details.preset_stream && e.details.preset_stream !== ''){x.preset_stream=' -preset '+e.details.preset_stream;}else{x.preset_stream=''}

        // if(e.details.stream_vcodec==='h264_vaapi'){
        //     x.stream_video_filters=[]
        //     x.stream_video_filters.push('format=nv12,hwupload');
        //     if(e.details.stream_scale_x&&e.details.stream_scale_x!==''&&e.details.stream_scale_y&&e.details.stream_scale_y!==''){
        //         x.stream_video_filters.push('scale_vaapi=w='+e.details.stream_scale_x+':h='+e.details.stream_scale_y)
        //     }
    	// }
        // if(e.cudaEnabled && (e.details.stream_type === 'mjpeg' || e.details.stream_type === 'b64')){
        //     x.stream_video_filters.push('hwdownload,format=nv12')
        // }
        // //stream - video filter
        // if(e.details.svf && e.details.svf !== ''){
        //     x.stream_video_filters.push(e.details.svf)
        // }
        // if(x.stream_video_filters.length>0){
        //     x.stream_video_filters=' -vf "'+x.stream_video_filters.join(',')+'"'
        // }else{
        //     x.stream_video_filters=''
        // }
        // //stream - pipe build
        // if(e.details.input_map_choices&&e.details.input_map_choices.stream){
        //     //add input feed map
        //     // x.pipe += s.createFFmpegMap(e,e.details.input_map_choices.stream)
        // }
        
        x.output.main_stream.decode=false;
        x.output.main_stream.codec='';
        switch(e.details.stream_type){
            case'mp4':
                // x.cust_stream+=' -movflags +frag_keyframe+empty_moov+default_base_moof -metadata title="Poseidon Stream" -reset_timestamps 1'
                // if(e.details.stream_vcodec!=='copy'){
                    // if(x.dimensions && x.cust_stream.indexOf('-s ')===-1){x.cust_stream+=' -s '+x.dimensions}
                    // if(e.details.stream_quality && e.details.stream_quality !== '')x.cust_stream+=' -crf '+e.details.stream_quality;
                    // x.cust_stream+=x.preset_stream
                    // x.cust_stream+=x.stream_video_filters
                // }
                // x.pipe+=' -f mp4'+x.stream_acodec+x.stream_vcodec+x.cust_stream+' pipe:1';
                if (x.input_vcodec!=='h264') {x.output.main_stream.decode=true}
                x.output.main_stream.codec='h264';
                x.output.main_stream.sink='mp4mux ! fdsink fd=1';
            break;
            case'webm':
                if (x.input_vcodec==='vp8'||x.input_vcodec==='vp9') {x.output.main_stream.decode=false} else {x.output.main_stream.decode=true}
                x.output.main_stream.codec='vp8';
                x.output.main_stream.sink='webmmux ! fdsink fd=1';
            break;
            case'flv':
                // if(e.details.stream_vcodec!=='copy'){
                    // if(x.dimensions && x.cust_stream.indexOf('-s ')===-1){x.cust_stream+=' -s '+x.dimensions}
                    // if(e.details.stream_quality && e.details.stream_quality !== '')x.cust_stream+=' -crf '+e.details.stream_quality;
                    // x.cust_stream+=x.preset_stream
                    // x.cust_stream+=x.stream_video_filters
                // }
                // x.pipe+=' -f flv'+x.stream_acodec+x.stream_vcodec+x.cust_stream+' pipe:1';
                if (x.input_vcodec!=='h264') {x.output.main_stream.decode=true}
                x.output.main_stream.codec='h264';
                x.output.main_stream.sink='flvmux streamable=true ! fdsink fd=1';
            break;
            case'hls':
                // if(e.details.stream_vcodec!=='h264_vaapi'&&e.details.stream_vcodec!=='copy'){
                    // if(e.details.stream_quality && e.details.stream_quality !== '')x.cust_stream+=' -crf '+e.details.stream_quality;
                    // if(x.cust_stream.indexOf('-tune')===-1){x.cust_stream+=' -tune zerolatency'}
                    // if(x.cust_stream.indexOf('-g ')===-1){x.cust_stream+=' -g 1'}
                    // if(x.dimensions && x.cust_stream.indexOf('-s ')===-1){x.cust_stream+=' -s '+x.dimensions}
                    // x.cust_stream+=x.stream_video_filters
                // }
                //  hlssink2 playlist-length=5 target-duration=2 playlist-location=/dev/shm/streams/wJsVM5drId/lOIBwgD1Me/play.m3u8 location=/dev/shm/streams/wJsVM5drId/lOIBwgD1Me/s%0d.ts
                if (x.input_vcodec==='h264') {x.output.main_stream.decode=false} else {x.output.main_stream.decode=true}
                x.output.main_stream.codec='h264';
                x.output.main_stream.sink=gstreamer.hlssink_cmd
                                        +' max-files='+e.details.hls_max_files
                                        +' playlist-length='+e.details.hls_list_size
                                        +' target-duration='+e.details.hls_time
                                        +' playlist-location='+e.sdir
                                        +'s.m3u8 location='+e.sdir+'s%04d.ts';
                // x.pipe+=x.preset_stream+x.stream_acodec+x.stream_vcodec+' -f hls'+x.cust_stream+' -hls_time '+x.hls_time+' -hls_list_size '+x.hls_list_size+' -start_number 0 -hls_allow_cache 0 -hls_flags +delete_segments+omit_endlist "'+e.sdir+'s.m3u8"';
            break;
            case'mjpeg':
                if(e.coProcessor === false){
                    // if(e.details.stream_quality && e.details.stream_quality !== '')x.cust_stream+=' -q:v '+e.details.stream_quality;
                    // if(x.dimensions && x.cust_stream.indexOf('-s ')===-1){x.cust_stream+=' -s '+x.dimensions}
                    // x.pipe+=' -an -c:v mjpeg -f mpjpeg -boundary_tag shinobi'+x.cust_stream+x.stream_video_filters+' pipe:1';
                    x.output.main_stream.decode=true;
                    x.output.main_stream.codec='jpeg';
                    // x.output.main_stream.codec='jpeg';
                    // x.output.main_stream.sink='nvvidconv ! videorate max-rate=1 ! nvjpegenc ! multipartmux ! fdsink fd=1';
                    x.output.main_stream.sink='multipartmux boundary=shinobi ! fdsink fd=1';
                }
            break;
            case'h265':
                // x.cust_stream+=' -movflags +frag_keyframe+empty_moov+default_base_moof -metadata title="Shinobi H.265 Stream" -reset_timestamps 1'
                // if(e.details.stream_vcodec!=='copy'){
                //     if(x.dimensions && x.cust_stream.indexOf('-s ')===-1){x.cust_stream+=' -s '+x.dimensions}
                //     if(e.details.stream_quality && e.details.stream_quality !== '')x.cust_stream+=' -crf '+e.details.stream_quality;
                //     x.cust_stream+=x.preset_stream
                //     x.cust_stream+=x.stream_video_filters
                // }
                // x.pipe+=' -f hevc'+x.stream_acodec+x.stream_vcodec+x.cust_stream+' pipe:1';
                if (x.input_vcodec==='h265') {
                    x.output.main_stream.decode=false
                } else {
                    x.output.main_stream.decode=true
                }
                
                x.output.main_stream.codec='h265';
                x.output.main_stream.sink='mpegtsmux ! fdsink fd=1';
            break;
            case'b64':case'':case undefined:case null://base64
                // if(e.coProcessor === false){
                //     if(e.details.stream_quality && e.details.stream_quality !== '')x.cust_stream+=' -q:v '+e.details.stream_quality;
                //     if(x.dimensions && x.cust_stream.indexOf('-s ')===-1){x.cust_stream+=' -s '+x.dimensions}
                //     x.pipe+=' -an -c:v mjpeg -f image2pipe'+x.cust_stream+x.stream_video_filters+' pipe:1';
                // }
                x.output.main_stream.decode=true;
                x.output.main_stream.codec='jpeg';
                x.output.main_stream.sink='jifmux ! fdsink fd=1';
                // x.output.main_stream.sink=x.input_stream.video+' ! '+gstreamer.plugins.encoder.jpeg+' ! rtpjpegpay ! fdsink fd=1';
            break;
            default:
                x.output.main_stream.sink=''
            break;
        }


        caps = '';
        if(e.details.stream_vcodec!=='copy'){
            x.output.main_stream.decode=true;
            // x.output.main_stream.sink='';
            
            // if(e.details.stream_bitrate!=='')x.cust_stream+=' -crf '+e.details.stream_quality;
        }
        
        if (x.output.main_stream.decode && x.output.main_stream.sink !== '') {
            if(e.details.stream_fps && e.details.stream_fps!=='' && e.details.stream_fps!==x.input_fps){
                x.output.main_stream.fps= e.details.stream_fps;
            }
            if (e.details.stream_scale_x!=='' && e.details.stream_scale_y!=='') {
                x.output.main_stream.scale_caps = 'video/x-raw, width='+e.details.stream_scale_x+', height='+e.details.stream_scale_y
            }
        }

        if (x.output.main_stream.sink==='') {
            delete x.output.main_stream;
        } 
        
        if(e.details.stream_channels){
            e.details.stream_channels.forEach(function(v,n){
                // if(v.stream_type === 'mjpeg')e.coProcessor = true;
                // x.pipe += s.createStreamChannel(e,n+config.pipeAddition,v)
            })
        }
        //api - snapshot bin/ cgi.bin (JPEG Mode)
        if(e.details.snap === '1' && e.coProcessor === false){
            if(e.details.input_map_choices&&e.details.input_map_choices.snap){
                //add input feed map
                // x.pipe += s.createFFmpegMap(e,e.details.input_map_choices.snap)
            }
            if(!e.details.snap_fps || e.details.snap_fps === ''){e.details.snap_fps = 1}
            if(e.details.snap_vf && e.details.snap_vf !== '' || e.cudaEnabled){
                // var snapVf = e.details.snap_vf.split(',')
                if(e.details.snap_vf === '')snapVf.shift()
                if(e.cudaEnabled){
                    // snapVf.push('hwdownload,format=nv12')
                }
                //-vf "thumbnail_cuda=2,hwdownload,format=nv12"
                // x.snap_vf=' -vf "'+snapVf.join(',')+'"'
            }else{
                // x.snap_vf=''
            }
            // if(e.details.snap_scale_x && e.details.snap_scale_x !== '' && e.details.snap_scale_y && e.details.snap_scale_y !== ''){x.snap_ratio = ' -s '+e.details.snap_scale_x+'x'+e.details.snap_scale_y}else{x.snap_ratio=''}
            // if(e.details.cust_snap && e.details.cust_snap !== ''){x.cust_snap = ' '+e.details.cust_snap}else{x.cust_snap=''}

            // x.pipe+=' -update 1 -r '+e.details.snap_fps+x.cust_snap+x.snap_ratio+x.snap_vf+' "'+e.sdir+'s.jpg" -y';
            x.output.stream_image.decode=true;
            x.output.stream_image.codec='jpeg';
            x.output.stream_image.sink='multifilesink location="'+e.sdir+'s.jpg"'
        }
        //custom - output
        if(e.details.custom_output&&e.details.custom_output!==''){x.output.main_stream.sink=e.details.custom_output;}
    }
    gstreamer.buildMainRecording = function(e,x){
        //e = monitor object
        //x = temporary values
        x.record_video_filters = []
        x.output.record = {}
        x.output.segment = {}
        x.record_string = ''
        x.eos_append_meta = ''
        x.recording_muxer = ''
        
       
        if(e.details.stream_scale_x&&e.details.stream_scale_x!==''&&e.details.stream_scale_y&&e.details.stream_scale_y!==''){
            x.dimensions = e.details.stream_scale_x+'x'+e.details.stream_scale_y;
        }
        //record - segmenting
        // x.segment = ' -f segment -segment_format_options movflags=faststart+frag_keyframe+empty_moov -segment_atclocktime 1 -reset_timestamps 1 -strftime 1 -segment_list pipe:2 -segment_time '+(60*e.cutoff)+' "'+e.dir+'%Y-%m-%dT%H-%M-%S.'+e.ext+'"';
        
        x.output.segment.decode=false;
        //record - set defaults for extension, video quality
        switch(e.ext){
            case'mp4':
                // x.eos_append_meta = '-e ';
                x.output.record.muxer = 'mp4mux';
                x.output.record.codec=x.input_vcodec;
                x.output.record.acodec='aac';
            break;
            case'webm':
                if (x.input_vcodec!=='vp8' && x.input_vcodec!=='vp9') {x.output.record.decode=true};
                x.output.record.muxer = 'webmmux';
                x.output.record.codec='vp8';
                x.output.record.acodec='vorbis';
                // if(e.details.crf&&e.details.crf!==''){x.vcodec+=' -q:v '+e.details.crf}else{x.vcodec+=' -q:v 1';}
            break;
            case'ts':
            // adding supprot for mpeg muxer to allo playing currently recorderd streams and playing partial files
                x.output.record.muxer = 'mpegtsmux';
                x.output.record.codec=x.input_vcodec;
                // if(e.details.crf&&e.details.crf!==''){x.vcodec+=' -q:v '+e.details.crf}else{x.vcodec+=' -q:v 1';}
            break;
        }

        // splitmuxsink doesn't support timestamp formatting. as a workaround a mode.js wrapper script will watch for splitmuxsink-fragment-opened
        // and override the original location value with a filen name that includes a timestamp.
        // a better solution would be to use a python wrapper script with on-pad-sink callback (synchronously) to override the location property value        
        x.output.record.sink='splitmuxsink name=splitmux max-size-time='+(60000000000*e.cutoff)
                            +' muxer='+x.output.record.muxer
                            +' location=\''+e.dir+'%06d.'+e.ext+'\'';

        
        //record - use custom video codec
        if(e.details.vcodec&&e.details.vcodec!==''&&e.details.vcodec!=='copy'){
            // x.vcodec=e.details.vcodec
            x.output.record.decode=true;
            x.output.record.codec=e.details.vcodec;

        }
        //record - use custom audio codec
        if(e.details.acodec&&e.details.acodec!==''&&e.details.acodec!=='default'){
            // x.acodec=e.details.acodec
        }

        if (x.output.record.decode && x.output.record.sink !== '') {
            //record - frames per second (fps)
            if(e.details.stream_fps && e.details.stream_fps!=='' && e.details.stream_fps!==x.input_fps){
                x.output.record.fps= e.fps;
            }
            //record - resolution
            if (e.details.record_scale_x!=='' && e.details.record_scale_y!=='') {
                x.output.record.scale_caps = 'video/x-raw, width='+e.details.stream_scale_x+', height='+e.details.stream_scale_y
            }
        }
        if(e.details.cust_record){
            // if(x.acodec=='aac'&&e.details.cust_record.indexOf('-strict -2')===-1){
            //     e.details.cust_record+=' -strict -2';
            // }
            // if(e.details.cust_record.indexOf('-threads')===-1){
            //     e.details.cust_record+=' -threads 1';
            // }
        }
    //    if(e.details.cust_input&&(e.details.cust_input.indexOf('-use_wallclock_as_timestamps 1')>-1)===false){e.details.cust_input+=' -use_wallclock_as_timestamps 1';}
        //record - ready or reset codecs
        if(x.acodec!=='no'){
            // if(x.acodec.indexOf('none')>-1){x.acodec=''}else{x.acodec=' -acodec '+x.acodec}
        }else{
            // x.acodec=' -an'
        }
        
        //record - timestamp options for -vf
        if(e.details.timestamp&&e.details.timestamp=="1"&&e.details.vcodec!=='copy'){
            //font
            // if(e.details.timestamp_font&&e.details.timestamp_font!==''){x.time_font=e.details.timestamp_font}else{x.time_font='/usr/share/fonts/truetype/freefont/FreeSans.ttf'}
            //position x
            // if(e.details.timestamp_x&&e.details.timestamp_x!==''){x.timex=e.details.timestamp_x}else{x.timex='(w-tw)/2'}
            //position y
            // if(e.details.timestamp_y&&e.details.timestamp_y!==''){x.timey=e.details.timestamp_y}else{x.timey='0'}
            //text color
            // if(e.details.timestamp_color&&e.details.timestamp_color!==''){x.time_color=e.details.timestamp_color}else{x.time_color='white'}
            //box color
            // if(e.details.timestamp_box_color&&e.details.timestamp_box_color!==''){x.time_box_color=e.details.timestamp_box_color}else{x.time_box_color='0x00000000@1'}
            //text size
            // if(e.details.timestamp_font_size&&e.details.timestamp_font_size!==''){x.time_font_size=e.details.timestamp_font_size}else{x.time_font_size='10'}

            // x.record_video_filters.push('drawtext=fontfile='+x.time_font+':text=\'%{localtime}\':x='+x.timex+':y='+x.timey+':fontcolor='+x.time_color+':box=1:boxcolor='+x.time_box_color+':fontsize='+x.time_font_size);
        }
        //record - watermark for -vf
        if(e.details.watermark&&e.details.watermark=="1"&&e.details.watermark_location&&e.details.watermark_location!==''){
            // switch(e.details.watermark_position){
            //     case'tl'://top left
            //         x.watermark_position='10:10'
            //     break;
            //     case'tr'://top right
            //         x.watermark_position='main_w-overlay_w-10:10'
            //     break;
            //     case'bl'://bottom left
            //         x.watermark_position='10:main_h-overlay_h-10'
            //     break;
            //     default://bottom right
            //         x.watermark_position='(main_w-overlay_w-10)/2:(main_h-overlay_h-10)/2'
            //     break;
            // }
            // x.record_video_filters.push('movie='+e.details.watermark_location+'[watermark],[in][watermark]overlay='+x.watermark_position+'[out]');
        }
        //record - rotation
        if(e.details.rotate_record&&e.details.rotate_record!==""&&e.details.rotate_record!=="no"&&e.details.stream_vcodec!=="copy"){
            // x.record_video_filters.push('transpose='+e.details.rotate_record);
        }
        //check custom record filters for -vf
        if(e.details.vf&&e.details.vf!==''){
            // x.record_video_filters.push(e.details.vf)
        }
        //compile filter string for -vf
        if(x.record_video_filters.length>0){
        //    x.record_video_filters=' -vf '+x.record_video_filters.join(',')
        }else{
            x.record_video_filters=''
        }
        //build record string.
        if(e.mode === 'record'){
            if(e.details.input_map_choices&&e.details.input_map_choices.record){
                //add input feed map
                // x.record_string += s.createFFmpegMap(e,e.details.input_map_choices.record)
            }
            //if h264, hls, mp4, or local add the audio codec flag
            switch(e.type){
                case'h264':case'hls':case'mp4':case'local':
                    // x.record_string+=x.acodec;
                break;
            }
            //custom flags
            // if(e.details.cust_record&&e.details.cust_record!==''){x.record_string+=' '+e.details.cust_record;}
            //preset flag
            // if(e.details.preset_record&&e.details.preset_record!==''){x.record_string+=' -preset '+e.details.preset_record;}
            //main string write
            // x.record_string+=x.vcodec+x.record_fps+x.record_video_filters+x.record_dimensions+x.segment;
            // x.record_string+=''+x.segment;
        }
    }
    gstreamer.buildAudioDetector = function(e,x){
        if(e.details.detector_audio === '1'){
            if(e.details.input_map_choices&&e.details.input_map_choices.detector_audio){
                //add input feed map
                // x.pipe += s.createFFmpegMap(e,e.details.input_map_choices.detector_audio)
            }else{
                // x.pipe += ' -map 0:a'
            }
            // x.pipe += ' -acodec pcm_s16le -f s16le -ac 1 -ar 16000 pipe:6'
        }
    }
    gstreamer.buildMainDetector = function(e,x){
        //e = monitor object
        //x = temporary values
        x.output.segment_detector_stream={};
        x.output.segment_detector_stream.decode=true;
        x.output.segment_detector={};
        x.output.segment_detector.sink='';
        x.output.segment_detector.decode=false;
        x.output.main_detector={};
        x.output.main_detector.sink='';
        x.output.main_detector.decode=false;
        x.output.main_detector.scale_cap='';
        x.output.main_detector.fps='';
        
        //detector - plugins, motion
        var sendFramesGlobally = (e.details.detector_send_frames === '1')
        var sendFramesToObjectDetector = (e.details.detector_send_frames_object !== '0' && e.details.detector_use_detect_object === '1')
        if(e.details.detector === '1' && (sendFramesGlobally || sendFramesToObjectDetector) && e.coProcessor === false){
            if(sendFramesGlobally && e.details.input_map_choices && e.details.input_map_choices.detector){
                //add input feed map
                // x.pipe += s.createFFmpegMap(e,e.details.input_map_choices.detector)
            }
            if(e.details.detector_fps!==e.fps){
                x.output.segment_detector.fps = 2;
            }
            if(e.details.detector_scale_x !== x.input_height || e.details.detector_scale_y !== x.input_height){
                x.output.segment_detector.scale_caps='video/x-raw, width='+e.details.detector_scale_x+', height='+e.details.detector_scale_y+' ! '
            }
            // x.detector_vf = []
            // if(sendFramesGlobally && x.detector_vf.length > 0)x.pipe += ' -vf "'+x.detector_vf.join(',')+'"'
            
            if (x.input_vcodec !== 'h264') {
                if (x.output.segment_detector.fps) {x.output.segment_detector_stream.fps=x.output.segment_detector.fps}
                if (x.output.segment_detector.scale_caps) {x.output.segment_detector_stream.scale_caps=x.output.segment_detector.scale_caps}
                x.output.segment_detector_stream.decode=true;
                x.output.segment_detector_stream.codec='h264';
            }
            var h264Output = gstreamer.hlssink_cmd+' name=segment_detector_stream ! max-files=3 playlist-length=3 target-duration=2 playlist-location="'+e.sdir+'detectorStreamX.m3u8" location="'+e.sdir+'detectorStreamX00%d.ts"'

            if(e.details.detector_pam === '1'){
                // if(sendFramesGlobally)x.pipe += ' -an -c:v pam -pix_fmt gray -f image2pipe pipe:3'
                if(sendFramesGlobally){
                    x.output.segment_detector_pam.decode=true;
                    // x.output.segment_detector.codec='pam';
                    x.output.segment_detector_pam.sink = 'avenc_pam ! image/x-portable-anymap, format=GRAY8 ! fdsink name=segment_detector_pam fd=3';
                }
                if(e.details.detector_use_detect_object === '1'){
                    //for object detection
                    // x.pipe += s.createFFmpegMap(e,e.details.input_map_choices.detector)
                    if(e.details.detector_scale_x_object&&e.details.detector_scale_x_object!==''&&e.details.detector_scale_y_object&&e.details.detector_scale_y_object!==''){x.dobjratio=', width='+e.details.detector_scale_x_object+', height='+e.details.detector_scale_y_object}else{x.dobjratio=x.dratio}
                    if(e.details.detector_h264 === '1'){
                        x.output.segment_detector_stream.sink= h264Output
                    }else{
                        x.output.segment_detector.decode=true;
                        x.output.segment_detector.codec='jpeg';
                        x.output.segment_detector.sink = 'fdsink name=segment_detector fd=4'
                    }
                }
            }else if(sendFramesGlobally){
                if(e.details.detector_h264 === '1'){
                    x.output.segment_detector_stream.sink= ' '+h264Output
                }else{
                    // x.pipe += ' -an -f singlejpeg pipe:3'
                    x.output.segment_detector.decode=true;
                    x.output.segment_detector.codec='jpeg';
                    x.output.segment_detector.sink = 'fdsink name=segment_detector fd=3'
                }
            }
        }
        //Traditional Recording Buffer
        if(e.details.detector=='1'&&e.details.detector_trigger=='1'&&e.details.detector_record_method==='sip'){
            // if(e.details.cust_sip_record && e.details.cust_sip_record !== ''){x.pipe += ' ' + e.details.cust_sip_record}
            if(e.details.input_map_choices&&e.details.input_map_choices.detector_sip_buffer){
                //add input feed map
                // x.pipe += s.createFFmpegMap(e,e.details.input_map_choices.detector_sip_buffer)
            }
            x.detector_buffer_filters=[]
            if(!e.details.detector_buffer_vcodec||e.details.detector_buffer_vcodec===''||e.details.detector_buffer_vcodec==='auto'){
                switch(e.type){
                    case'h264':case'hls':case'mp4':
                        e.details.detector_buffer_vcodec = 'copy';
                        x.output.main_detector.decode=false;
                    break;
                    default:
                            x.output.main_detector.codec='h264';
                            x.output.main_detector.decode=true;
                    break;
                }
            }
            // if(!e.details.detector_buffer_acodec||e.details.detector_buffer_acodec===''||e.details.detector_buffer_acodec==='auto'){
            //     switch(e.type){
            //         case'mjpeg':case'jpeg':case'socket':
            //             e.details.detector_buffer_acodec = 'no'
            //         break;
            //         case'h264':case'hls':case'mp4':
            //             e.details.detector_buffer_acodec = 'copy'
            //         break;
            //         default:
            //             e.details.detector_buffer_acodec = 'aac'
            //         break;
            //     }
            // }
            if(e.details.detector_buffer_acodec === 'no'){
                x.detector_buffer_acodec = ' -an'
            }else{
                x.detector_buffer_acodec = ' -c:a '+e.details.detector_buffer_acodec
            }
            if(!e.details.detector_buffer_tune||e.details.detector_buffer_tune===''){e.details.detector_buffer_tune='zerolatency'}
            if(!e.details.detector_buffer_g||e.details.detector_buffer_g===''){e.details.detector_buffer_g='1'}
            if(!e.details.detector_buffer_hls_time||e.details.detector_buffer_hls_time===''){e.details.detector_buffer_hls_time='2'}
            if(!e.details.detector_buffer_hls_list_size||e.details.detector_buffer_hls_list_size===''){e.details.detector_buffer_hls_list_size='4'}
            if(!e.details.detector_buffer_start_number||e.details.detector_buffer_start_number===''){e.details.detector_buffer_start_number='0'}
            if(!e.details.detector_buffer_live_start_index||e.details.detector_buffer_live_start_index===''){e.details.detector_buffer_live_start_index='-3'}

            if(e.details.detector_buffer_vcodec!=='copy'){
                if(e.details.detector_buffer_fps&&e.details.detector_buffer_fps!==''){
                    x.detector_buffer_fps=' -r '+e.details.detector_buffer_fps
                }else{
                    x.detector_buffer_fps=' -r 30'
                }
            }else{
                x.detector_buffer_fps=''
            }
            if(x.detector_buffer_filters.length>0){
                // x.pipe+=' -vf '+x.detector_buffer_filters.join(',')
            }
            // main_input. ! queue ! hlssink max-files=4 playlist-length=4 target-duration=2 playlist-location=/dev/shm/streams/wJsVM5drId/lOIBwgD1Me/detectorStream.m3u8 location=/dev/shm/streams/wJsVM5drId/lOIBwgD1Me/detectorStream%04d.ts
            x.output.main_detector.sink= gstreamer.hlssink_cmd+' name=main_detector max-files='+e.details.detector_buffer_hls_list_size+' playlist-length='+e.details.detector_buffer_hls_list_size+' target-duration='+e.details.detector_buffer_hls_time+' playlist-location="'+e.sdir+'detectorStreamX.m3u8" location="'+e.sdir+'detectorStreamX00%d.ts"'
            // x.pipe += x.detector_buffer_fps+x.detector_buffer_acodec+' -c:v '+e.details.detector_buffer_vcodec+' -f hls -tune '+e.details.detector_buffer_tune+' -g '+e.details.detector_buffer_g+' -hls_time '+e.details.detector_buffer_hls_time+' -hls_list_size '+e.details.detector_buffer_hls_list_size+' -start_number '+e.details.detector_buffer_start_number+' -live_start_index '+e.details.detector_buffer_live_start_index+' -hls_allow_cache 0 -hls_flags +delete_segments+omit_endlist "'+e.sdir+'detectorStream.m3u8"'
            // Debugging hack
            // x.sink= gstreamer.input_start_cmd+' ! '+gstreamer.decoder_cmd+' ! '+gstreamer.plugins.videoconverter+' ! video/x-raw, width=640, height=480 ! videorate ! video/x-raw, framerate=5/1 ! nvjpegenc ! fdsink fd=3 '+gstreamer.input_name+'. ! queue ! hlssink max-files=4 playlist-length=4 target-duration=2 playlist-location='+e.sdir+'detectorStream.m3u8 location='+e.sdir+'detectorStream%04d.ts'
        }
    }
    gstreamer.buildCoProcessorFeed = function(e,x){
        if(e.coProcessor === true){
            // the coProcessor ffmpeg consumes this HLS stream (no audio, frames only)
            // x.pipe += ' -q:v 1 -an -c:v copy -f hls -tune zerolatency -g 1 -hls_time 2 -hls_list_size 3 -start_number 0 -live_start_index 3 -hls_allow_cache 0 -hls_flags +delete_segments+omit_endlist "'+e.sdir+'coProcessor.m3u8"'
        }
    }
    gstreamer.buildTimelapseOutput = function(e,x){
        if(e.details.record_timelapse === '1'){
            x.record_timelapse_video_filters = []
            if(e.details.input_map_choices&&e.details.input_map_choices.record_timelapse){
                //add input feed map
                x.pipe += s.createFFmpegMap(e,e.details.input_map_choices.record_timelapse)
            }
            var flags = []
            if(e.details.record_timelapse_fps && e.details.record_timelapse_fps !== ''){
                flags.push('-r 1/' + e.details.record_timelapse_fps)
            }else{
                flags.push('-r 1/900') // 15 minutes
            }
            if(e.details.record_timelapse_vf && e.details.record_timelapse_vf !== '')flags.push('-vf ' + e.details.record_timelapse_vf)
            if(e.details.record_timelapse_scale_x && e.details.record_timelapse_scale_x !== '' && e.details.record_timelapse_scale_y && e.details.record_timelapse_scale_y !== '')flags.push(`-s ${e.details.record_timelapse_scale_x}x${e.details.record_timelapse_scale_y}`)
            //record - watermark for -vf
            if(e.details.record_timelapse_watermark&&e.details.record_timelapse_watermark=="1"&&e.details.record_timelapse_watermark_location&&e.details.record_timelapse_watermark_location!==''){
                switch(e.details.record_timelapse_watermark_position){
                    case'tl'://top left
                        x.record_timelapse_watermark_position='10:10'
                    break;
                    case'tr'://top right
                        x.record_timelapse_watermark_position='main_w-overlay_w-10:10'
                    break;
                    case'bl'://bottom left
                        x.record_timelapse_watermark_position='10:main_h-overlay_h-10'
                    break;
                    default://bottom right
                        x.record_timelapse_watermark_position='(main_w-overlay_w-10)/2:(main_h-overlay_h-10)/2'
                    break;
                }
                x.record_timelapse_video_filters.push('movie='+e.details.record_timelapse_watermark_location+'[watermark],[in][watermark]overlay='+x.record_timelapse_watermark_position+'[out]');
            }
            if(x.record_timelapse_video_filters.length > 0){
                var videoFilter = `-vf "${x.record_timelapse_video_filters.join(',').trim()}"`
                flags.push(videoFilter)
            }
            x.pipe += ` -f singlejpeg ${flags.join(' ')} -an -q:v 1 pipe:7`
        }
    }
    gstreamer.assembleMainPieces = function(e,x){
        //create executeable FFMPEG command
        // x.ffmpegCommandString = x.loglevel+x.input_fps;
        //progress pipe
        // x.ffmpegCommandString += ' -progress pipe:5';
        
        // x.ffmpegCommandString += x.eos_append_meta+x.cust_input+x.input_parse;
        
        //add extra input maps
        if(e.details.input_maps){
            e.details.input_maps.forEach(function(v,n){
                // x.ffmpegCommandString += s.createInputMap(e,n+1,v)
            })
        }
        x.GstreamerPipeline = '';
        if (gstreamer.use_wrapper === true) {
            x.GstreamerPipeline = 'libs/gst-launch-1.0.js';
        }

        x.GstreamerPipeline+=' '+x.main_input;
        // x.GstreamerPipeline+=' '+gstreamer.input_start;
        for(var output in x.output) {
            if (!x.output[output].sink||x.output[output].sink==='') {continue}
            if (x.output[output].decode) {
                if (!x.stream_decoded) {
                    x.stream_decoded=gstreamer.input_start+' ! '+gstreamer.plugins.decoder[x.input_vcodec]+' ! tee name='+gstreamer.decoded_name;
                    x.GstreamerPipeline+=' '+x.stream_decoded;
                }
                encoder='';
                if (x.output[output].encoder) {
                    encoder=x.output[output].encoder;
                }
                else if (x.output[output].codec){
                    // workaround for nvv4l2h265enc and mp4mux not supporting the same format
                    if (x.output[output].codec==='h265' && gstreamer.plugins.encoder[x.output[output].codec]==='nvv4l2h265enc' && x.output[output].sink.indexOf('mp4mux')!==-1) {
                        encoder='omxh265enc';
                    }
                    else {
                        encoder=gstreamer.plugins.encoder[x.output[output].codec];
                    }
                }
                x.GstreamerPipeline+=' '+gstreamer.input_decoded_start
                if (x.output[output].fps) {x.GstreamerPipeline+=' ! '+gstreamer.plugins.videorate+' max-rate='+x.output[output].fps}
                if (x.output[output].scale_caps) {' ! '+gstreamer.plugins.videoscale+' ! '+x.output[output].scale_caps}
                x.GstreamerPipeline+=' ! '+encoder
            }
            else {
                x.GstreamerPipeline+=' '+gstreamer.input_start
            }
            x.GstreamerPipeline+=' ! '+x.output[output].sink
        }
        //add recording and stream outputs
        // ideally a single parse on the input (x.input_parse) is prefered, however, for some reason splitmuxsink hangs when splitting
        // the input to different streams while muxer=mp4mux; moving the parser after the queue seems to resulve the problem for now
        // tee=' '+gstreamer.input_name+'. ! queue'+x.queue_parse+'! '
        // if (x.pipe !== '') {
        //     x.ffmpegCommandString += tee+x.pipe;
        // }
        // x.ffmpegCommandString += x.record_string
    }
    gstreamer.createPipeArray = function(e,x){
        //create additional pipes from ffmpeg
        x.stdioPipes = [];
        var times = config.pipeAddition;
        if(e.details.stream_channels){
            times+=e.details.stream_channels.length
        }
        for(var i=0; i < times; i++){
            x.stdioPipes.push('pipe')
        }
    }
    s.ffmpeg = function(e){
        //set X for temporary values so we don't break our main monitor object.
        var x = {tmp : ''}
        //set some placeholding values to avoid "undefined" in ffmpeg string.
        gstreamer.buildMainInput(e,x)
        gstreamer.buildMainStream(e,x)
        gstreamer.buildMainRecording(e,x)
        gstreamer.buildAudioDetector(e,x)
        gstreamer.buildMainDetector(e,x)
        gstreamer.buildCoProcessorFeed(e,x)
        gstreamer.buildTimelapseOutput(e,x)
        s.onFfmpegCameraStringCreationExtensions.forEach(function(extender){
            extender(e,x)
        })
        gstreamer.assembleMainPieces(e,x)
        gstreamer.createPipeArray(e,x)
        //hold ffmpeg command for log stream
        s.group[e.ke].activeMonitors[e.mid].ffmpeg = x.GstreamerPipeline
        //clean the string of spatial impurities and split for spawn()
        x.GstreamerPipeline = s.splitForFFPMEG(x.GstreamerPipeline)
        //launch that bad boy
        // {stdio: "inherit"}
        
        return spawn(config.gstDir,x.GstreamerPipeline,{detached: false,stdio:x.stdioPipes})
        // return spawn(config.ffmpegDir,x.ffmpegCommandString,{detached: true,stdio:"inherit"})
    }
    if(!config.ffmpegDir){
        ffmpeg.checkForWindows(function(){
            ffmpeg.checkForFfbinary(function(){
                ffmpeg.checkForNpmStatic(function(){
                    ffmpeg.checkForUnix(function(){
                        console.log('No FFmpeg found.')
                    })
                })
            })
        })
    }
    if(downloadingFfmpeg === false){
        //not downloading ffmpeg
        ffmpeg.completeCheck()
    }
    return ffmpeg
}
