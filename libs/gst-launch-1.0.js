var fs = require('fs');

var progress_report = fs.createWriteStream("/tmp/tmp", {fd: 5});

const gstreamer = require('gstreamer-superficial');


var pipeline_args = process.argv.slice(2);

path = '';
ext = '';
plugin = '';
current_file = '';
previous_file = '';
start = true;
count = 0;
c = 0;
offset = 0;
name_timer = 3;
init = true;
const location_regexp = /^(location=)(.*)$/;
const splitmux_regexp = /^([^\!]+\/)%06d\.([^\.]+)$/;
const interval_regex = /^(max-size-time=)(.*)$/;
const value_regexp = /^['"]{0,1}([^"']*)['"]{0,1}$/;



function handle(signal) {
	console.error(previous_file);
	pipeline.sendEOS();
	pipeline.stop();
	process.exit();
}


process.on('SIGINT', handle);
process.on('SIGTERM', handle);


function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

function appendLeadingZeroes(n){
	if(n <= 9){
		return "0" + n;
	}
	return n
}

get_timestamp = function(offset) {
	let current_datetime = new Date();
	if (offset) {
		current_datetime.setSeconds(current_datetime.getSeconds() - name_timer + offset);
	}
	let formatted_date = current_datetime.getFullYear() + "-" + appendLeadingZeroes(current_datetime.getMonth() + 1) + "-" + appendLeadingZeroes(current_datetime.getDate()) + "T" + appendLeadingZeroes(current_datetime.getHours()) + "-" + appendLeadingZeroes(current_datetime.getMinutes()) + "-" + appendLeadingZeroes(current_datetime.getSeconds())
	return formatted_date;
	
}

get_filename = function(path, ext, offset) {
	if (current_file != '') {
		previous_file = current_file;
	}
	current_file = get_timestamp(offset)+'.'+ext;
	return path+current_file;
}

pipeline_args.forEach(function (item, index) {
	if (item === '-e') {
		return
	}
	if (start === true) {
		plugin = item;
		start = false;
	}
	else if (item === '!') {
		start = true;
	}
	else {
		if (match = item.match(location_regexp)) {
			location = match[1];
			value = match[2];
			if (match = value.match(value_regexp)) {
				value = match[1];
			}

			if (plugin === 'splitmuxsink') {
				let match = value.match(splitmux_regexp);
				path = match[1];
				ext = match[2];
				value = get_filename(path, ext);
			}
			pipeline_args[index] = 'location="'+value+'"';
		};
		if (match = item.match(interval_regex)) {
			offset = match[2] / 1000000000;
		}
	}
});

var pipeline_cmd = pipeline_args.join(' ');

const pipeline = new gstreamer.Pipeline(pipeline_cmd);


pipeline.play();

const target = pipeline.findChild('splitmux');

pipeline.pollBus(msg => {
	if (msg.name&&msg.name.match(/.*progress.*/)) {
		progress_report.write("playing for: "+msg.current+" seconds\n");
	}
	if ((msg.name) && (msg.name === 'splitmuxsink-fragment-opened') && (typeof target !== 'undefined') && (msg.location.startsWith(path))) {
		count++;
		// gstreamer-superficial does not support synchronous callbacks, so instead using a dirty hack to set the location of the next timestamp file
		// otherwise we'll run into race condition. a better implementation will be to use a python wrapper script instead.
		setTimeout( function() {target.location = get_filename(path, ext, offset);}, name_timer * 1000);
	}
	if ((msg.name) && (msg.name === 'splitmuxsink-fragment-closed') && (typeof target !== 'undefined') && (msg.location.startsWith(path))) {

		console.error(previous_file);
	}
	if (msg.type) {
		switch( msg.type ) {
			case 'eos':
				pipeline.stop();
				break;
		}
	}
	
});


